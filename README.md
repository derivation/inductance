# Inductance

Un outil pour gérer des tâches administratives pour La Dérivation.
Il est très spécifique à notre activité et à notre organisation.

Pour l'instant, il permet d’automatiser la création des factures
à partir des paiements effectués avec Stripe, de générer des
colonnes de récapitulatif prête à coller dans le tableau mensuel
récapitulant les ventes en ligne de Coopaname, et d'envoyer tout
ça dans un dossier Nextcloud.

## Installation

```
sudo apt install ruby curl

curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt install nodejs

sudo gem install bundler

cd inductance
bundle config set --local path vendor/bundle
bundle install
npm install
```

## Configuration

La configuration se fait dans le fichier `settings.yml`.

Le fichier `settings.sample.yml` est fourni à titre d'exemple.

## Usage

```
bin/inductance
```

En cas de soucis avec Grover, Chromium et le *sandboxing*, utiliser :

```
GROVER_NO_SANDBOX=true bin/inductance
```

## Licence

Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de détails.

Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.
