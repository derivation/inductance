# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

require 'erb'
require 'singleton'
require 'stripe'

ADDRESS_TEMPLATE = <<END_OF_TEMPLATE
<% if line1 -%>
<%= line1 %>
<% end -%>
<% if line2 -%>
<%= line2 %>
<% end -%>
<%= postal_code %> <%= city %>
<% if "FR" != country -%>
<%= country %>
<% end -%>
END_OF_TEMPLATE

module Inductance
  class Stripe
    include Singleton

    def setup
      ::Stripe.api_key = Inductance.settings.stripe_api_key
    end

    def paid_charges_each_in_month(date)
      charges = ::Stripe::Charge.list({created: {gte: date.first_day_of_month.strftime('%s'), lt: date.next_month.first_day_of_month.strftime('%s')}})
      charges.auto_paging_each do |charge|
        if charge.paid
          yield charge
        end
      end
    end

    def line_items_each(checkout_session)
      line_items = ::Stripe::Checkout::Session.list_line_items(checkout_session.id)
      line_items.auto_paging_each do |line_item|
        yield line_item
      end
    end

    def get_checkout_session(charge)
      checkout_sessions = ::Stripe::Checkout::Session.list(payment_intent: charge.payment_intent)
      if checkout_sessions.count > 1
        puts "/!\ More than one checkout session for payment #{payment.id}."
        puts "Only handling the first one."
      end
      checkout_sessions.first
    end

    def add_items(checkout_session, checkout)
      previous_line = nil
      line_items_each(checkout_session) do |line_item|
        if line_item.description.include?("TVA")
          checkout.add_item(
            Inductance::Checkout::Item.new(
              description: previous_line.description,
              quantity: previous_line.quantity,
              unit_price: previous_line.price.unit_amount,
              tax_description: line_item.description,
              tax_amount: line_item.amount_total
            )
          )
          previous_line = nil
        elsif previous_line.nil?
          previous_line = line_item
        else
          checkout.add_item(
            Inductance::Checkout::Item.new(
              description: previous_line.description,
              quantity: previous_line.quantity,
              unit_price: previous_line.price.unit_amount
            )
          )
        end
      end
      unless previous_line.nil?
        checkout.add_item(
          Inductance::Checkout::Item.new(
            description: previous_line.description,
            quantity: previous_line.quantity,
            unit_price: previous_line.price.unit_amount
          )
        )
      end
    end

    def get_balance_transaction(charge)
      ::Stripe::BalanceTransaction.retrieve(charge.balance_transaction)
    end

    def refund(charge)
      if charge.refunds.total_count > 1
        STDERR.write "Unable to unable multiple refunds yet."
        exit 1
      end

      return nil unless charge.refunded

      refund = charge.refunds.data[0]
      Inductance::Checkout::Refund.new(created: refund.created)
    end

    def get_monthly_checkouts(date)
      checkouts = []
      paid_charges_each_in_month(date) do |charge|
        billing_details = charge.billing_details
        balance_transaction = get_balance_transaction(charge)
        checkout = Inductance::Checkout.new(
          created: Date.strptime(charge.created.to_s, '%s'),
          billing_details: Inductance::BillingDetails.new(
            recipient: billing_details.name,
            email: billing_details.email,
            address: ERB.new(ADDRESS_TEMPLATE, nil, '-').result(
              billing_details.address.instance_eval { binding }
            ),
            country: billing_details.address&.country || 'FR'
          ),
          refund: refund(charge),
          stripe_fee: balance_transaction.fee,
        )
        checkout_session = get_checkout_session(charge)
        if checkout_session.nil?
          puts "No CheckoutSession for charge #{charge.id}. Skipping."
          next
        end
        add_items(checkout_session, checkout)
        # assert payment.amount == checkout total
        checkouts << checkout
      end
      checkouts
    end
  end
end
