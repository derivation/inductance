# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

require 'date'

class Date
  def first_day_of_month
    Date.new(self.year, self.mon, 1)
  end
end
