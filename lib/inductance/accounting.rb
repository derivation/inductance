# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

require 'erb'

MONTHLY_RECAP_COLUMN_TEMPLATE = <<END_OF_TEMPLATE
      <div class="column">
        <table>
          <thead>
            <tr>
              <th><%= title %></th>
            </tr>
          </thead>
          <tbody>
            <% items.each do |item| %>
              <tr><td class="<%= css_class %>"><%= block.call(item) %></td></tr>
            <% end %>
          </tbody>
          <tfoot><!-- Empty but used to look up the end of the selection range --></tfoot>
        </table>
      </div>
END_OF_TEMPLATE

MONTHLY_RECAP_TEMPLATE = <<END_OF_TEMPLATE
<!doctype html>
<html>
  <head>
    <meta charset='utf-8' />
    <style>
:root {
  --main-text-color: black;
  --secondary-text-color: hsla(0, 0%, 25%, 1);
  --highlight-text-color: hsla(195, 50%, 50%, 1);
  --table-background-odd: hsla(195, 50%, 90%, 1);
  --table-background-even: hsla(195, 50%, 95%, 1);
  --table-background-highlight: hsla(195, 50%, 70%, 1);
}

body {
  background: white;
  color: var(--main-text-color);
  font-family: "Arial", sans-serif;
  font-size: 16px;
  line-height: 1.33;
}

.help {
  border: solid #333 1px;
  background: lightblue;
  display: flex;
  align-items: center;
  width: min-content;
}

.help:before {
  content: "🛈";
  font-size: 300%;
  line-height: 1;
  padding-bottom: 0.2ch;
  margin-right: 0.2rem;
}

.help span {
  min-width: 50ch;
}
main {
  display: flex;
  flex-wrap: wrap;
}

hr {
  color: black;
  border-style: solid;
  margin: 0;
}

.column {
  margin: 1rem;
}

.done {
  opacity: 0.3;
}

table {
  border-spacing: 0;
  border: solid black 2px;
  width: 100%;
}

th {
  border-bottom: solid black 2px;
  background: #0066cc;
  color: white;
}

td {
  border-bottom: solid black 1px;
  text-align: right;
  background: #99ccff;
}

tr:last-child td {
  border-bottom: none;
}

td.label {
  text-align: left;
}

.notification {
  position: absolute;
  background: lightyellow;
  color: black;
  border: solid #333 1px;
  padding: 0.5ch;
  animation: 1s ease-in disappear;
}

@keyframes disappear {
  0% { opacity: 1; }
  70% { opacity: 1; }
  100% { opacity: 0 }
}
    </style>
    <script>
document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('.column').forEach(divColumn => {
    divColumn.addEventListener('click', function(event) {
      // Select our column and copy it into clipboard
      window.getSelection().collapse(divColumn.querySelector("tbody > tr > td"));
      window.getSelection().extend(divColumn.querySelector("tfoot"));
      document.execCommand("copy");
      window.getSelection().removeAllRanges();

      // Handle user feedback
      divColumn.classList.add("done");
      let previousNotification = document.querySelector(".notification");
      if (previousNotification) {
        previousNotification.remove();
      }
      let notification = document.createElement("div");
      notification.classList.add("notification");
      notification.innerText = "C’est copié, reste plus qu'à coller ! (Shif+Ctrl+Alt+V)";
      notification.style.top = `${event.pageY - 15}px`;
      notification.style.left = `${event.pageX + 15}px`;
      document.body.append(notification);
      setTimeout(() => { notification.remove() }, 1000);
    });
  });
});
    </script>
  </head>
  <body>
    <header>
      <h1>Aide au remplissage du tableau mensuel</h1>
      <h2><%= date.strftime("%B %Y") %></h2>
      <p class="help"><span>Cliquer sur une colonne pour la copier dans le presse-papier.</span></p>
    </header>
    <main>
      <%= column("Date") { |i| i.invoice.created.strftime('%d/%m/%Y') } %>
      <%= column("Nom du client", "label") { |i| i.invoice.billing_details.recipient } %>
      <%= column("Pays de résidence du client", "label") { |i| i.invoice.billing_details.country } %>
      <%= column("N° facture") { |i| i.invoice.invoice_id } %>
      <%= column("Total Facture TTC") { |i| i.invoice.total.as_euros_without_symbol } %>
      <%= column("PRODUITS FACTURÉS SELON TAUX TVA<hr/>HT 20%") { |i| i.invoice.total_without_taxes.as_euros_without_symbol } %>
      <%= column("Règlements<hr/>Date") { |i| i.invoice.created } %>
      <%= column("Règlements<hr/>Moyen de paiement", "label") { |_, _| "STRIPE" } %>
      <%= column("Frais de commissions diverses<hr/>Montant TTC") { |i| i.invoice.stripe_fee.as_euros_without_symbol } %>
    </main>
  </body>
</html>
END_OF_TEMPLATE

module Inductance
  module Accounting
    class << self
      def monthly_recap(date, invoices)
        items = invoices.collect do |invoice|
          Item.new(invoice: invoice)
        end.sort_by { |i| i.invoice.created }
        ERB.new(MONTHLY_RECAP_TEMPLATE).result(
          MonthlyTemplate.new(
            date: date,
            items: items
          ).instance_eval { binding }
        )
      end
    end

    private

    class MonthlyTemplate < Struct.new(:date, :items, keyword_init: true)
      def column(title, css_class = "", &block)
        @@erb ||= ERB.new(MONTHLY_RECAP_COLUMN_TEMPLATE)
        @@binding_class ||= Struct.new(:title, :css_class, :items, :block)
        @@erb.result(@@binding_class.new(title, css_class, items, block).instance_eval { binding })
      end
    end

    class Item < Struct.new(:invoice, keyword_init: true)
    end
  end
end
