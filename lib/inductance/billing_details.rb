# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

module Inductance
  class BillingDetails < Struct.new(:recipient, :email, :address, :country, keyword_init: true)
  end
end
