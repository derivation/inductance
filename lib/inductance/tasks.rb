# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

module Inductance
  module Tasks
    class << self
      def process_monthly_stripe_checkouts
        puts "🕛 Setting up"
        date = default_month
        webdav = Inductance::Nextcloud.webdav
        base_folder = webdav.directory.find(Inductance.settings.webdav.folder)
        unless base_folder.class == ::Nextcloud::Models::Directory
          raise "Unable to lookup base folder #{Inductance.settings.webdav.folder}"
        end

        puts "🕐 Getting checkouts from Stripe"
        checkouts = Inductance::Stripe.instance.get_monthly_checkouts(date)
        puts "🕑 Calculating invoices"
        invoices = Inductance::Invoice::invoice_checkouts(checkouts)

        month_folder = "#{Inductance.settings.webdav.folder}#{date.strftime("%Y-%m")}/"
        puts "🕒 Creating folder “#{month_folder}”"
        webdav.directory.create(month_folder)

        puts "🕓 Uploading invoices"
        invoices.each do |invoice|
          puts "   📄 #{invoice.invoice_id}"
          webdav.directory.upload(
            "#{month_folder}#{invoice.invoice_id.gsub(/[^[:alnum:]]/, '-')}.pdf",
            invoice.to_pdf
          )
        end

        puts "🕓 Uploading monthly recap"
        webdav.directory.upload(
          "#{month_folder}monthly-recap.html",
          Inductance::Accounting.monthly_recap(date, invoices)
        )

        puts "🎆 We are done here!"
      end

      def default_month
        today = Date.today
        Date.new(today.year,
                 if today.mday >= 7 then today.month else today.month - 1 end,
                 1)
      end
    end
  end
end
