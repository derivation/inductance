# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

module Inductance
  class Checkout < Struct.new(:created, :billing_details, :stripe_fee, :refund, keyword_init: true)
    class Item < Struct.new(:description, :quantity, :unit_price, :tax_description, :tax_amount, keyword_init: true)
    end

    class Refund < Struct.new(:created, keyword_init: true)
    end

    def refunded?
      @refund.nil?
    end

    def items
      @items.freeze
    end

    def add_item(item)
      @items ||= []
      @items << item
    end
  end
end
