# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

require 'erb'

require 'grover'

Grover.configure do |config|
  config.options = {
    format: 'A4',
    print_background: true,
    margin: {
      top: '1cm',
      right: '2cm',
      bottom: '1cm',
      left: '2cm',
    }
  }
end

INVOICE_TEMPLATE = <<END_OF_TEMPLATE
<!doctype html>
<html>
  <head>
    <meta charset='utf-8' />
    <style>
:root {
  --main-text-color: black;
  --secondary-text-color: hsla(0, 0%, 25%, 1);
  --highlight-text-color: hsla(195, 50%, 50%, 1);
  --table-background-odd: hsla(195, 50%, 90%, 1);
  --table-background-even: hsla(195, 50%, 95%, 1);
  --table-background-highlight: hsla(195, 50%, 70%, 1);
}

body {
  margin: 0;
  padding: 0;
  background: white;
  color: var(--main-text-color);
  font-family: "Arial", sans-serif;
  display: grid;
  min-height: calc(27.7cm);
  grid-template-rows: auto 1fr auto;
  font-size: 12pt;
  line-height: 1.33;
}

header {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
}

header h1 {
  width: 60%;
  font-size: 24pt;
  margin: 1cm 0;
}

header h2 {
  width: 40%;
  font-size: 24pt;
  color: var(--highlight-text-color);
  text-align: right;
  margin: 1cm 0;
}

header dl {
  order: 2;
  display: grid;
  grid-template-columns: auto auto;
  row-gap: 1ex;
  margin: 0;
}

header dt {
  color: var(--secondary-text-color);
}

header dd {
  margin-left: 1ch;
}

header ul {
  margin: 0;
}

.from {
  list-style: none;
  padding-left: 0;
}

.to {
  list-style: none;
  padding-left: 0;
}

.address {
  white-space: pre;
}

main {
  margin: 2cm auto;
}

main table {
  width: 18cm;
  border-spacing: 0;
}

main th {
  color: var(--secondary-text-color);
}

main th,
main td {
  padding: 0.7ex 1ch;
  text-align: right;
}

main th.label,
main td.label {
  text-align: left;
  max-width: 10cm;
}

main thead tr:last-child th,
main tbody tr:last-child td {
  border-bottom: solid 2px var(--table-background-highlight);
}

main tbody tr:nth-child(2n + 1) td {
  background: var(--table-background-odd);
}
main tbody tr:nth-child(2n) td {
  background: var(--table-background-even);
}

main tfoot tr:nth-child(2n) th,
main tfoot tr:nth-child(2n) th + td {
  background: var(--table-background-even);
}

main tfoot tr:last-child th,
main tfoot tr:last-child th + td {
  border-top: solid 2px var(--table-background-highlight);
  background: var(--table-background-odd);
}

footer {
  border-top: solid 1px var(--highlight-text-color);
  text-align: center;
}
    </style>
  </head>
  <body>
    <header>
      <h1><%= Inductance.settings.brand %></h1>
      <h2>
        Facture
        <% if total_without_taxes < 0 %>
        d’avoir
        <% end %>
      </h2>
      <dl>
        <dt>Numéro de facture</dt>
        <dd><%= invoice_id %></dd>
        <dt>Date d’émission</dt>
        <dd><%= created.strftime('%Y-%m-%d') %></dd>
        <dt>État</dt>
        <dd>Payée</dd>
        <dt>Destinataire</dt>
        <dd>
          <ul class="to">
            <li class="recipient"><%= billing_details.recipient %></li>
            <li class="address"><%= billing_details.address %></li>
            <li class="email"><%= billing_details.email %></li>
          </ul>
        </dd>
      </dl>
      <ul class="from">
        <li class="address"><%= Inductance.settings.address %></li>
        <li class="phone"><%= Inductance.settings.phone %></li>
        <li class="email"><%= Inductance.settings.email %></li>
      </ul>
    </header>
    <main>
      <% if comment %>
        <p><%= comment %></p>
      <% end %>
      <table>
        <thead>
          <tr>
            <th class="label" scope="col">Description</td>
            <th scope="col">Quantité</td>
            <th scope="col">Prix unitaire</td>
            <th scope="col">Montant</td>
          </tr>
        </thead>
        <tbody>
          <% items.each do |item| %>
            <tr>
              <td class="label"><%= item.description %></td>
              <td><%= item.quantity %></td>
              <td><%= item.unit_price.as_euros %></td>
              <td><%= item.line_total.as_euros %></td>
            </tr>
          <% end %>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <th scope="rowgroup">Sous-total</th>
            <td><%= total_without_taxes.as_euros %></td>
          </tr>
          <% taxes.each do |tax| %>
            <tr>
              <td colspan="2"></td>
              <th scope="rowgroup"><%= tax.description %></th>
              <td><%= tax.total.as_euros %></td>
            </tr>
          <% end %>
          <tr>
            <td colspan="2"></td>
            <th scope="rowgroup">Montant total</th>
            <td><strong><%= total.as_euros %></strong></td>
          </tr>
        </tfoot>
      </table>
    </main>
    <footer>
      <%= Inductance.settings.footer %>
    </footer>
  </body>
</html>
END_OF_TEMPLATE

module Inductance
  class Invoice < Struct.new(:invoice_id, :created, :billing_details, :stripe_fee, :comment, keyword_init: true)
    class << self
      def invoice_checkouts(checkouts)
        checkouts.group_by { |c| c.created.strftime('%y%m') }.flat_map do |month, monthly_checkouts|
          refunds = []
          monthly_checkouts.sort_by(&:created).zip((1..Integer::MAX)).collect do |checkout, number|
            invoice_id = sprintf("%s%s%04d", Inductance.settings.invoice_id_prefix, month, number)
            if checkout.refund
              refunds << { :checkout => checkout, :original_invoice_id => invoice_id }
            end
            from_checkout(invoice_id, checkout)
          end.union(
            refunds.sort_by { |r| r[:checkout].refund.created }.zip((1..Integer::MAX)).collect do |refund, number|
              invoice_id = sprintf("%sRE%s%04d", Inductance.settings.invoice_id_prefix, month[2..], number)
              from_refund(invoice_id, refund[:original_invoice_id], refund[:checkout])
            end
          )
        end
      end

      def from_checkout(invoice_id, checkout)
        invoice = Invoice.new(
          invoice_id: invoice_id,
          stripe_fee: checkout.stripe_fee,
          created: checkout.created,
          billing_details: checkout.billing_details
        )
        checkout.items.each do |item|
          invoice.add_item(Item.new(description: item.description,
                                    quantity: item.quantity,
                                    unit_price: item.unit_price))
        end
        checkout.items.group_by { |item| item.tax_description }.each do |tax_description, items|
          next if tax_description.nil?
          invoice.add_tax(Tax.new(description: tax_description,
                                  total: items.collect { |item| item.tax_amount }.sum))
        end
        invoice
      end

      def from_refund(invoice_id, original_invoice_id, checkout)
        invoice = from_checkout(invoice_id, checkout)
        invoice.created = Date.strptime(checkout.refund.created.to_s, format='%s')
        invoice.comment = "Annulation et remboursement de la facture #{original_invoice_id} du #{checkout.created.strftime("%Y-%m-%d")}."
        invoice.refund_items!
        invoice.refund_taxes!
        invoice
      end
    end

    class Item < Struct.new(:description, :quantity, :unit_price, keyword_init: true)
      def line_total
        quantity * unit_price
      end
    end
    class Tax < Struct.new(:description, :total, keyword_init: true)
    end

    def add_item(item)
      @items ||= []
      @items << item
    end

    def add_tax(tax)
      @taxes ||= []
      @taxes << tax
    end

    def items
      (@items || []).freeze
    end

    def refund_items!
      @items.each do |item|
        item.unit_price = -item.unit_price
      end
    end

    def taxes
      (@taxes || []).freeze
    end

    def refund_taxes!
      @taxes.each do |tax|
        tax.total = -tax.total
      end
    end

    def total_without_taxes
      (@items || []).collect { |item| item.line_total }.sum
    end

    def total
      total_without_taxes + (@taxes || []).collect { |tax| tax.total }.sum
    end

    def to_html
      ERB.new(INVOICE_TEMPLATE).result(binding)
    end

    def to_pdf
      Grover.new(to_html).to_pdf
    end

    def save_as_pdf(path)
      Grover.new(to_html).to_pdf(path)
    end
  end
end
