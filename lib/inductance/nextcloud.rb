# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

require 'nextcloud'

module Inductance
  module Nextcloud
    class << self
      def webdav
        ::Nextcloud.webdav(
          url: Inductance.settings.webdav.url,
          username: Inductance.settings.webdav.username,
          password: Inductance.settings.webdav.password,
        )
      end
    end
  end
end
