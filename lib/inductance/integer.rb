# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

class Integer
  N_BYTES = [42].pack('i').size
  N_BITS = N_BYTES * 16
  MAX = 2 ** (N_BITS - 2) - 1
  MIN = -MAX - 1

  def as_euros
    sprintf("%d,%02d €", self / 100, self % 100)
  end

  def as_euros_without_symbol
    sprintf("%d,%02d", self / 100, self % 100)
  end
end
