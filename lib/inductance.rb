# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

#-*- coding: utf-8 -*-

require 'ostruct'
require 'yaml'

require 'inductance/date'
require 'inductance/integer'
require 'inductance/billing_details'
require 'inductance/checkout'
require 'inductance/stripe'
require 'inductance/invoice'
require 'inductance/accounting'
require 'inductance/nextcloud'
require 'inductance/tasks'

SETTINGS_FILE = 'settings.yml'

module Inductance
  class << self
    attr_reader :settings

    def load_settings
        @settings = OpenStruct.new(YAML::safe_load(File.read(SETTINGS_FILE)))
        @settings.webdav = OpenStruct.new(@settings.webdav).freeze
        @settings.freeze
    end
  end
end
