# SPDX-FileCopyrightText: 2021 Lunar <lunar@derivation.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

Gem::Specification.new do |s|
  s.name        = 'inductance'
  s.summary     = 'Handle administrative tasks for La Dérivation'
  s.authors     = ['ecrire@derivation.fr']
  s.version     = '0.0.1.dev'
  s.require_paths = ['lib']
  s.license     = 'AGPL-3+'

  s.add_runtime_dependency 'stripe'
  s.add_runtime_dependency 'pdfkit'
  s.add_runtime_dependency 'grover'
  s.add_runtime_dependency 'nextcloud'
end
